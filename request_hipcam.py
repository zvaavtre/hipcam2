import requests
from flask import Flask
from flask_responses import json_response

app = Flask(__name__)


def get_hipcam_obj():
    hipcam_obj = requests.get("https://developer-api.nest.com/devices?auth=c.3Ines9b66Ug39utv9pbLjEZsK0lFZzxtCCilWihWnPioEiqLu8APA41PHrsongMZ7MohLP2bomIasNDlciTh48FiHeJFwotrod7KpvlILbplbr5J7LvkSIjpeZmIpePD41LcTLdO5Hqo6SSR")
    return hipcam_obj.json()

def get_hipcam_camera_objects():
    hipcam_json = get_hipcam_obj()
    return hipcam_json['cameras']

def get_camera_1_obj():
    cameras = get_hipcam_camera_objects()
    cam1 = cameras.values()[0]
    return cam1

def get_camera_1_last_event_object():
    cam_1_url = get_camera_1_obj()
    cam_1_last_event = cam_1_url['last_event']
    return cam_1_last_event
    # for k,v in cam_1_url:
    #     if 'Camera 1' == k:
    #         print v


def get_camera_animated_url():
    event_obj = get_camera_1_last_event_object()
    print event_obj
    return  event_obj['image_url']


print get_camera_animated_url()


@app.route("/")
def main():
    url = get_camera_animated_url()
    image_format =  """<img src="{}" alt="table status image" style="width:250px;height:250px;">""".format(url)
    print image_format
    test = {'image': image_format}
    return image_format


if __name__ == "__main__":
    app.run('0.0.0.0', debug=True, port=5000, ssl_context=('/Users/pratikshah/Projects/hipchat/hipcam/server.crt', '/Users/pratikshah/Projects/hipchat/hipcam/server.key'))



