
$(function(){

    $('body')

        // Delete User
        .on('click', '#table_status', function(event){
            event.preventDefault();

            var $el = $(this),
                user_id = $el.data('id'),
                $row = $el.closest('tr');

            $.ajax({
                type: 'POST',
                url: '/users/delete',
                data: {
                    user_id : user_id
                },
                datatype: 'json',
                success: function(data) {
                    console.log(data.status);
                    console.log(data.message);
                    if (Number(data) === 1 || data.status == true)
                    {


                        AJS.messages.success({
                            title: 'Success!',
                            body: 'User has been deleted.',
                            fadeout: true
                        });
                        $row.fadeOut(function(){
                            $row.remove();
                        });
                        window.updateUserCount(-1);
                    }
                    else if ((Number(data) === 1 || data.status == false))
                    {
                        AJS.messages.error({
                            title: 'Oops! There was an error with that request!',
                            body: data.message,
                            fadeout: true
                        });
                    }
                },

                error: function(data) {
                    console.log(data.status);
                    console.log(data.message);

                    console.log(data.message);
                    AJS.messages.error({
                        title: 'There was a problem.',
                        body: 'Uknown error. See toolbox server log.',
                        fadeout: true
                    });
                }
            });
        })



});