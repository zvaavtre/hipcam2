## Getting this to work

For local developement

* Install docker
* Boot up a redis docker container:  `docker run --name my-redis -d -p 6379:6379 redis`
* Install ngrok to tunnell public https back to your local machine
* Start ngrok:  `ngrok http 3000`
* Take the url it offers and put it in the config.json
* NOW you can run the addon:  `cd hipcam; node app.js`
* Go to your hipchat room -> Add an integration -> Build your own. Find the link to submit your url for the descriptor and copy that ngrok endpoint from the config.
* Now HC can see your local node addon.


Nest Setup

* Go to developer.nest.com
* Setup a new product. 
    * Leave the redirect URI blank. Want to use the PIN method
* From the product page
    * Use the Auth URL in a public place (say the add on config page) so the user can follow it to generate a PIN code
    * Your service then uses the user's pin and the Access token URL to get an access token, just put the pin code in the right spot.
* The access token you get back is what you use for all the api calls FOR THAT USER (follow redirects)


## Next steps

To Get a video/image for a particular room integraiton

* Store the groupID, roomID and access token in your db.
* When the sidbar makes a request back to your service pass along the groupId, roomId.  
* Use those to lookup the access token and make your request to nest
* Return image url (or video url) to the sidebar